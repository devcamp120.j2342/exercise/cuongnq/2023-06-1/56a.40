package devcamp.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.models.Circle;

//Khai báo là 1 controller
@RestController
//Khai báo tiền tố của api
@RequestMapping ("/")
//Khai báo phạm vi server có thể truy cập
@CrossOrigin

public class democontroller {
        //Khai báo api dạng Get data
        @GetMapping("/circle-area")
        //Function diễn giải cho API /rainbow
        public String getCicle(@RequestParam Double radius) {
            Circle circle1 = new Circle(radius);
            return circle1.toString();

        }
    
}
